Aplicação feita no framework Python Flask com base na documentação do meu [TCC](https://github.com/souovan/TCC)

# Para executar a aplicação Flask

Com o banco de dados já em execução:

```sh
git clone https://github.com/souovan/TCC-python.git
cd tcc-python/python-flask/src/

python3 -m venv venv
source venv/bin/activate

pip install -r requirements.txt

flask run
```
